module.exports = {
  mongodbMemoryServerOptions: {
    instance: {
      dbName: 'hnfeed_test',
    },
    binary: {
      version: '4.2.0',
      skipMD5: true,
    },
    autoStart: false,
  },
};
