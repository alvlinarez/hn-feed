import { Module } from '@nestjs/common';
import { DeletedArticlesService } from './deleted-articles.service';
import { MongooseModule } from "@nestjs/mongoose";
import { DeletedArticlesSchema } from "./deleted-articles.model";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'DeletedArticle', schema: DeletedArticlesSchema }
    ])
  ],
  providers: [DeletedArticlesService]
})
export class DeletedArticlesModule {}
