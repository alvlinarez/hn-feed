import * as mongoose from 'mongoose';

export const DeletedArticlesSchema = new mongoose.Schema({
  objectID: {
    type: String,
    unique: true,
  },
  title: String,
  author: String,
  story_title: String,
  created_at: String,
});

export interface DeletedArticle extends mongoose.Document {
  objectID: string;
  title: string;
  author: string;
  story_title: string;
  created_at: string;
}
