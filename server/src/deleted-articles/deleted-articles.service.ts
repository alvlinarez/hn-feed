import { Injectable } from '@nestjs/common';

import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";

import { DeletedArticle } from "./deleted-articles.model";

@Injectable()
export class DeletedArticlesService {
  constructor(@InjectModel('DeletedArticle') private readonly deletedArticleModel: Model<DeletedArticle>) {}

  async insertDeletedArticle(
    objectID: string,
    title: string,
    author: string,
    story_title: string,
    created_at: string,) {
    try {
      const newDeletedArticle = new this.deletedArticleModel({
        objectID,
        title,
        author,
        story_title,
        created_at
      });
      return await newDeletedArticle.save();
    } catch (e) {
      console.log(e);
      return null;
    }
  }
}
