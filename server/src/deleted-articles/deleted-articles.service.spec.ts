import * as mongoose from 'mongoose';
import { DeletedArticlesSchema } from './deleted-articles.model';

const DeletedArticle = mongoose.model('DeletedArticle', DeletedArticlesSchema);

describe('DeleteArticle Model Test', () => {
  let connection;
  // It's just so easy to connect to the MongoDB Memory Server
  // By using mongoose.connect
  beforeAll(async () => {
    connection = await mongoose.connect(
      'mongodb://localhost:27017/hnfeed_test',
      { useNewUrlParser: true, useCreateIndex: true },
      (err) => {
        if (err) {
          console.error(err);
          process.exit(1);
        }
      },
    );
  });

  it('should insert a doc into collection', async () => {
    await DeletedArticle.deleteMany({});

    const mockDeletedArticle = {
      objectID: '24987265',
      title: null,
      author: 'olie_h',
      story_title: 'Ask HN: Who is hiring? (November 2020)',
      created_at: '2020-11-04T08:21:33.000Z',
    };
    const deletedArticle = new DeletedArticle(mockDeletedArticle);
    await deletedArticle.save();

    const insertedDeletedArticle = await DeletedArticle.findOne({
      objectID: '24987265',
    }).select('-__v -_id');
    expect(insertedDeletedArticle.toJSON()).toEqual(mockDeletedArticle);
  });

  afterAll(async () => {
    await connection.close();
  });
});
