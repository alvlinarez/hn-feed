import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Article } from './articles.model';

@Injectable()
export class ArticlesService {
  constructor(
    @InjectModel('Article') private readonly articleModel: Model<Article>,
  ) {}

  async getArticles() {
    try {
      let articles = await this.articleModel.find({});
      articles = articles.sort(
        (a, b) =>
          (new Date(b.created_at) as any) - (new Date(a.created_at) as any),
      );
      return articles;
    } catch (e) {
      console.log(e);
      return null;
    }
  }

  async getArticleByObjectID(objectID: string) {
    try {
      return await this.articleModel.findOne({ objectID });
    } catch (e) {
      console.log(e);
      return null;
    }
  }

  async insertManyArticles(articles: []) {
    try {
      return await this.articleModel.insertMany(articles);
    } catch (e) {
      console.log(e);
      return null;
    }
  }

  async deleteArticle(objectID: string) {
    const result = await this.articleModel.deleteOne({ objectID });
    if (result.n === 0) {
      throw new NotFoundException('Could not find article.');
    }
    return true;
  }

  async deleteAll() {
    try {
      return await this.articleModel.deleteMany({});
    } catch (e) {
      console.log(e);
      return null;
    }
  }
}
