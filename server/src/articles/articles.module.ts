import { Module } from '@nestjs/common';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { MongooseModule } from "@nestjs/mongoose";
import { ArticlesSchema } from "./articles.model";
import { DeletedArticlesSchema } from "../deleted-articles/deleted-articles.model";
import { DeletedArticlesService } from "../deleted-articles/deleted-articles.service";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Article', schema: ArticlesSchema },
      { name: 'DeletedArticle', schema: DeletedArticlesSchema }
      ])
  ],
  controllers: [ArticlesController],
  providers: [ArticlesService, DeletedArticlesService]
})
export class ArticlesModule {}
