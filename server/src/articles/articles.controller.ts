import { Controller, Delete, Get, HttpStatus, Param } from '@nestjs/common';

import { ArticlesService } from './articles.service';
import { DeletedArticlesService } from '../deleted-articles/deleted-articles.service';

@Controller('articles')
export class ArticlesController {
  constructor(
    private readonly articlesService: ArticlesService,
    private readonly deletedArticlesService: DeletedArticlesService,
  ) {}

  @Get()
  async getAllArticles() {
    try {
      return await this.articlesService.getArticles();
    } catch (e) {
      console.log(e);
      return {
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'Getting articles error.',
      };
    }
  }

  @Delete(':objectID')
  async deleteArticle(@Param('objectID') objectID: string) {
    const article = await this.articlesService.getArticleByObjectID(objectID);
    if (!article) {
      return {
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'Could not found article.',
      };
    }
    const isDeleted = await this.articlesService.deleteArticle(objectID);
    if (isDeleted) {
      await this.deletedArticlesService.insertDeletedArticle(
        article.objectID,
        article.title,
        article.author,
        article.story_title,
        article.created_at,
      );
      return {
        statusCode: HttpStatus.OK,
        message: 'Article deleted successfully',
      };
    }
  }
}
