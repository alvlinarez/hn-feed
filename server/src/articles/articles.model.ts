import * as mongoose from 'mongoose';

export const ArticlesSchema = new mongoose.Schema({
  objectID: {
    type: String,
    unique: true
  },
  title: String,
  url: String,
  author: String,
  story_title: String,
  story_url: String,
  created_at: String
});

export interface Article extends mongoose.Document {
  objectID: string;
  title: string;
  url: string;
  author: string;
  story_title: string;
  story_url: string;
  created_at: string;
}
