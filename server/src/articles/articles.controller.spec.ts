import * as mongoose from 'mongoose';
import axios from 'axios';
import { ArticlesSchema } from './articles.model';
import { DeletedArticlesSchema } from '../deleted-articles/deleted-articles.model';

const DeletedArticle = mongoose.model('DeletedArticle', DeletedArticlesSchema);
const Article = mongoose.model('Article', ArticlesSchema);

const fetchData = async () => {
  const { data } = await axios.get(
    'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
  );
  return data;
};

describe('Article Model Test', () => {
  let connection;
  beforeAll(async () => {
    connection = await mongoose.connect(
      'mongodb://localhost:27017/hnfeed_test',
      { useNewUrlParser: true, useCreateIndex: true },
      (err) => {
        if (err) {
          console.error(err);
          process.exit(1);
        }
      },
    );
  });

  it('Get Articles', async () => {
    await Article.deleteMany({});
    const { hits } = await fetchData();
    await Article.insertMany(hits);

    const articles = await Article.find({});

    expect(articles).toHaveLength(20);
  });

  it('Deleting One Article', async () => {
    await DeletedArticle.deleteMany({});
    let articles = await Article.find({});

    const deletedArticle = new DeletedArticle({
      // @ts-ignore
      objectID: articles[0].objectID,
      // @ts-ignore
      title: articles[0].title,
      // @ts-ignore
      author: articles[0].author,
      // @ts-ignore
      story_title: articles[0].story_title,
      // @ts-ignore
      created_at: articles[0].created_at,
    });

    await deletedArticle.save();

    expect(articles).toHaveLength(20);

    // @ts-ignore
    await Article.deleteOne({ objectID: articles[0].objectID });
    articles = await Article.find({});
    expect(articles).toHaveLength(19);
  });

  afterAll(async () => {
    await connection.close();
  });
});
