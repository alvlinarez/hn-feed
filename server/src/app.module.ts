import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticlesModule } from './articles/articles.module';
import { DeletedArticlesModule } from './deleted-articles/deleted-articles.module';

@Module({
  imports: [
    //MongooseModule.forRoot('mongodb://localhost:27017/hnfeed'),
    MongooseModule.forRoot('mongodb://mongo:27017/hnfeed'),
    ArticlesModule,
    DeletedArticlesModule,
  ],
})
export class AppModule {}
