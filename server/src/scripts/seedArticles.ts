import * as mongoose from 'mongoose';
import axios from 'axios';
import { ArticlesSchema } from '../articles/articles.model';
import { DeletedArticlesSchema } from '../deleted-articles/deleted-articles.model';

const Article = mongoose.model('Article', ArticlesSchema);
const DeletedArticle = mongoose.model('DeletedArticle', DeletedArticlesSchema);

const hnApiUrl = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';

export const seedArticles = async () => {
  try {
    // options to avoid mongoose deprecated warnings
    //await mongoose.connect(`mongodb://localhost:27017/hnfeed`, {
    await mongoose.connect(`mongodb://mongo:27017/hnfeed`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });

    const { data } = await axios.get(hnApiUrl);

    let deletedArticles = await DeletedArticle.find({}).select('objectID -_id');
    // @ts-ignore
    deletedArticles = deletedArticles.map((art) => art.objectID);

    // Filter articles that have been deleted before
    const articles = data.hits.filter((hit) => {
      if (!deletedArticles.includes(hit.objectID)) {
        const elem = {
          objectID: '',
          title: '',
          url: '',
          author: '',
          story_title: '',
          story_url: '',
          created_at: '',
        };
        elem.objectID = hit.objectID;
        elem.title = hit.title;
        elem.url = hit.url;
        elem.author = hit.author;
        elem.story_title = hit.story_title;
        elem.story_url = hit.story_url;
        elem.created_at = hit.created_at;
        return elem;
      }
    });

    // Rebooting collections
    await Article.deleteMany({});

    // Inserting filtered articles from HN API
    await Article.insertMany(articles);

    console.log('Articles have been updated successfully.');
  } catch (e) {
    console.log(e);
    console.error('Updating articles error.');
  }
};
