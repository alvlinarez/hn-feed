import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { seedArticles } from './scripts/seedArticles';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  // seed DB with HN API
  seedArticles();
  // Update articles every hour
  setInterval(() => {
    seedArticles();
  }, 3600 * 1000);
  await app.listen(5000);
}
bootstrap();
