# hn-feed
Web application that lists Hacker News articles 
in chronological order.

Using React, Nestjs and MongoDB

![hn-feed Capture](.readme-static/Capture.png)


####Commands to run web application:


**Client**

cd client

docker build -t hn-feed:client

**Server**

cd server

docker build -t node-app

docker-compose up -d --build


