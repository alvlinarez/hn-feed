import React from "react";
import axios from "axios";
import { isYesterday, isToday, format } from "date-fns";
import { API_URL } from "../utils/apiUrl";
import "../styles/components/Article.css";

interface ArticleProps {
  article: any;
  articles: any;
  setArticles: any;
}

const Article = ({ article, articles, setArticles }: ArticleProps) => {
  const {
    title,
    story_title,
    url,
    author,
    story_url,
    objectID,
    created_at,
  } = article;
  const exploreArticle = () => {
    if (!story_url && !url) {
      alert(`Sorry, there is no link for this article :(`);
      return;
    }
    window.open(story_url ? story_url : url, "_blank");
  };
  const handleDeleteArticle = async () => {
    try {
      const { data } = await axios.delete(API_URL + `/${objectID}`);
      articles = articles.filter((art: any) => art.objectID !== objectID);
      setArticles(articles);
      alert(data.message);
    } catch (e) {
      console.log(e);
    }
  };

  const formatDate = () => {
    if (isYesterday(new Date(created_at))) {
      return "Yesterday";
    }
    if (isToday(new Date(created_at))) {
      return format(new Date(created_at), "p").toLowerCase();
    }
    return format(new Date(created_at), "MMM d");
  };

  return (
    <div className="articleContainer">
      <div className="articleLeftContainer" onClick={exploreArticle}>
        <div className="articleTitle">
          {story_title !== null ? story_title : title}
        </div>
        <div className="articleAuthor">- {author} -</div>
      </div>

      <div className="articleRightContainer">
        <div className="articleDate">{formatDate()}</div>
        <div className="articleDelete">
          <span>
            <i className="fas fa-trash-alt" onClick={handleDeleteArticle} />
          </span>
        </div>
      </div>
    </div>
  );
};

export default Article;
