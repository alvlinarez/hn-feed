import React from "react";
import "../styles/components/Loader.css";

const Loader = () => {
  return (
    <div className="loaderContainer">
      <div className="ldsRing">
        <div />
        <div />
        <div />
        <div />
      </div>
    </div>
  );
};

export default Loader;
