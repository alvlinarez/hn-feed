import React, { useEffect, useState } from "react";
import axios from "axios";
import Article from "./components/Article";
import Loader from "./components/Loader";
import { API_URL } from "./utils/apiUrl";
import "./styles/App.css";

export const App = () => {
  const [articles, setArticles] = useState<any[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<any>(null);

  const getArticles = async () => {
    try {
      const { data } = await axios.get(API_URL);
      setArticles(data);
      setLoading(false);
      setError(null);
    } catch (e) {
      setLoading(false);
      setError(true);
      setArticles([]);
    }
  };

  useEffect(() => {
    getArticles();
  }, []);

  return (
    <div>
      <section id="jumbotron">
        <div className="titleContainer">
          <div className="title">HN Feed</div>
          <div className="subtitle">{"We <3 hacker news!"}</div>
        </div>
      </section>

      <section id="content">
        {loading ? (
          <Loader />
        ) : error ? (
          <div className="error">An unexpected error happened :(</div>
        ) : (
          <div className="articles">
            {articles.map((article) => (
              <Article
                key={article._id}
                article={article}
                articles={articles}
                setArticles={setArticles}
              />
            ))}
          </div>
        )}
      </section>
    </div>
  );
};
export default App;
